// Learn cc.Class:
//  - [Chinese] http://docs.cocos.com/creator/manual/zh/scripting/class.html
//  - [English] http://www.cocos2d-x.org/docs/creator/en/scripting/class.html
// Learn Attribute:
//  - [Chinese] http://docs.cocos.com/creator/manual/zh/scripting/reference/attributes.html
//  - [English] http://www.cocos2d-x.org/docs/creator/en/scripting/reference/attributes.html
// Learn life-cycle callbacks:
//  - [Chinese] http://docs.cocos.com/creator/manual/zh/scripting/life-cycle-callbacks.html
//  - [English] http://www.cocos2d-x.org/docs/creator/en/scripting/life-cycle-callbacks.html

cc.Class({
    extends: cc.Component,

    properties: {
        // foo: {
        //     // ATTRIBUTES:
        //     default: null,        // The default value will be used only when the component attaching
        //                           // to a node for the first time
        //     type: cc.SpriteFrame, // optional, default is typeof default
        //     serializable: true,   // optional, default is true
        // },
        // bar: {
        //     get () {
        //         return this._bar;
        //     },
        //     set (value) {
        //         this._bar = value;
        //     }
        // },
    },

    // LIFE-CYCLE CALLBACKS:

    // onLoad () {},

    start() {

    },
    onBeginContact: function (contact, self, other) {
        if(self.node.name.indexOf("img_block") !== -1 || other.node.name.indexOf("img_block") !== -1){
            cc.audioEngine.play(cc.url.raw("resources/sound/hit.mp3"), false, 0.5);
            return;
        }
        let gameLogic = cc.director.getScene().getChildByName("Canvas").getComponent("GameLogic");
        if (self.node.name === "pole" || other.node.name === "pole") {  //和杆子碰撞
            gameLogic.gameStatus = 2;
        } else if (self.node.name.indexOf("egg") !== -1) {  //彩蛋
            let score = self.node.getChildByName("ScoreLabel").getComponent(cc.Label).string;
            self.node.active = false;
            gameLogic.egg(score);
        } else if (other.node.name.indexOf("egg") !== -1) {  //彩蛋
            let score = other.node.getChildByName("ScoreLabel").getComponent(cc.Label).string;
            other.node.active = false;
            gameLogic.egg(score);
        }else{
            gameLogic.gameStatus = 3;
        }

        console.log("当前状态:"+gameLogic.gameStatus);
        if (gameLogic.gameStatus === 3) {
            gameLogic.analysisResult();
        }
    }
    // update (dt) {},
});
