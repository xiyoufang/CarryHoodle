# 疯狂弹弹珠

#### 项目介绍

疯狂弹弹珠游戏软件类似弹珠机，通过点击开始按钮随机选定若干个得分区，然后通过控制力度与角度弹射弹珠、并运用物理引擎技术，弹珠将经过碰撞与飞行后进入指定的区域，若进入得分区则增加弹珠个数，否则扣除一个弹珠；最后会根据得到的弹珠数量生成好友排行榜。

![游戏截图](https://images.gitee.com/uploads/images/2018/0712/164432_73d2c61b_369917.png "s.png")

#### 软件架构

1. 使用Cocoscreator开发
2. 使用物理引擎实现碰撞与回弹等物理效果
3. 可运行于微信小游戏、浏览器、安卓、IOS等各种平台

#### 安装教程

1. 根目录为主项目
2. wx-open-data-project目录为子项目（无需排行功能则不用）
3. 使用Cocoscreator直接打开就行


#### 参与贡献

1. Fork 本项目
2. 新建 Feat_xxx 分支
3. 提交代码
4. 新建 Pull Request

#### 其他

1. 个人博客 [www.xiyoufang.com](https://www.xiyoufang.com) 获取更多软件开发信息


### 您也可以加入游戏开发交流QQ群：112958956 ，一起讨论游戏开发技术。

![输入图片说明](https://images.gitee.com/uploads/images/2018/0708/183503_d1f599f2_369917.png "temp_qrcode_share_112958956.png")